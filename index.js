//EX1
function Tinhdiem(){
    var diemchuan=document.getElementById("diemChuan").value*1;
    var diemvung=document.getElementById("selVung").value*1;
    var diemuutien=document.getElementById("selDoituong").value*1;
    var dToan=document.getElementById("diemToan").value*1;
    var dVan=document.getElementById("diemVan").value*1;
    var dAnh=document.getElementById("diemAnh").value*1;
    if(dToan==""||dVan==""||dAnh==""||diemchuan==""){
        document.getElementById("res1").innerHTML=`<span class="bg-warning text-white">Xin vui lòng nhập đủ dữ liệu!</span>`;
    }
    else{
        if(dToan==0 || dVan==0 || dAnh==0){
            document.getElementById("res1").innerHTML=`<span class="bg-secondary text-white">Bạn đã rớt kì thi tốt nghiệp!</span>`;
        }
        else{
            console.log(diemchuan);
            console.log(diemvung);
            console.log(diemuutien);
            console.log(dToan);
            console.log(dVan);
            console.log(dAnh);
            var tongdiem = dToan+dVan+dAnh+diemvung+diemuutien;
            if(tongdiem>=diemchuan){
            document.getElementById("res1").innerHTML=`<span class="bg-success">Xin chúc mừng! Bạn đã đậu tốt nghiệp và điểm của bạn là : ${tongdiem}</span>`;}
        
            else{
            document.getElementById("res1").innerHTML=`<span class="bg-secondary text-white">Thật đáng tiếc! Bạn đã rớt kì thi tốt nghiệp và điểm của bạn là: ${tongdiem}</span>`;}
        }
        }
    }
// EX2
function tinhTienDien() {
    var ten = document.getElementById("hoTen").value;
    var soKW = document.getElementById("sokw").value*1;
    var tienDien;
  
    if (soKW <= 50) {
      tienDien = soKW * 500;
    } else if (soKW <= 100) {
      tienDien = 50 * 500 + (soKW - 50) * 650;
    } else if (soKW <= 200) {
      tienDien = 50 * 500 + 50 * 650 + (soKW - 100) * 850;
    } else if (soKW <= 350){
      tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKW - 200) * 1100;
    }
    else{
        tienDien = 50 * 500 + 50 * 650 + 100 * 850 + 350 *1100 + (soKW - 350) *1300;
    }
    document.getElementById("res2").innerHTML=`Họ và tên: <span class="text-primary">${ten} </span> - Tiền điện: <span class="text-primary">${tienDien}</span> VND`;
}
// EX3 Bài này em làm theo định dạng tính % thuế theo điều kiện mà thu nhập chịu thuế rơi vào.
function tinhThue(){
    var hotenthue=document.getElementById("hoTen3").value;
    var thunhap=document.getElementById("tongthunhap").value*1;
    var songuoipt=document.getElementById("nguoiphuthuoc").value*1;
    var tnchiuthue=thunhap-4000000-(songuoipt*16000000);
    var tienthue;
    if(tnchiuthue<=60e+6){
        tienthue=tnchiuthue*0.05;
    }
    else if (tnchiuthue<=120e+6){
        tienthue=tnchiuthue*0.1;
    }
    else if (tnchiuthue<=210e+6){
        tienthue=tnchiuthue*0.15;
    }
    else if (tnchiuthue<=384e+6){
        tienthue=tnchiuthue*0.2;
    }
    else if (tnchiuthue<=624e+6){
        tienthue=tnchiuthue*0.25;
    }
    else if (tnchiuthue<=960e+6){
        tienthue=tnchiuthue*0.3;
    }
    else{
        tienthue=tnchiuthue*0.35;
    }
    tienthue=new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(tienthue);
    document.getElementById("res3").innerHTML=`Họ và tên: <span class="text-primary">${hotenthue} </span> - Số người phụ thuộc: <span class="text-primary">${songuoipt} </span> - Tổng thuế: <span class="text-primary">${tienthue} </span>`;
}
// EX4
function disable(){
    var x = document.getElementById("loaiKH").value;
    if (x=='user'|x==""){
        document.querySelector(".sokn").classList.add("d-none");
    }
    if (x=="compa"){
        document.querySelector(".sokn").classList.remove("d-none");
    }
}
function tinhtiencap(){
    var x = document.getElementById("loaiKH").value;
    var y = document.getElementById("maKH").value;
    var z = document.getElementById("kenhcaocap").value*1;
    var w = document.getElementById("soketnoi").value*1;
    var tongphi;
    console.log(x);
    console.log(y);
    console.log(z);
    console.log(w);

    if (x=="user"){
        tongphi = 4.5 + 20.5 + (7.5*z);
    }
    console.log(tongphi);
    if (x=="compa"){
        if(w<=10){
        tongphi = 15 + 7.5*w + (50*z);}
        else{
        tongphi = 15 + 75 + (w-10)*5 + (50*z);}
    }
    document.getElementById("res4").innerHTML=`Mã KH: <span class="text-primary">${y}</span> - Tổng tiền cáp: <span class="text-primary">${tongphi} $</span>`;
}
